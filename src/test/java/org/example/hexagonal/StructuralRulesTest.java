package org.example.hexagonal;

import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

@AnalyzeClasses(packages = StructuralRulesTest.PACKAGE, importOptions = { ImportOption.DoNotIncludeTests.class })
class StructuralRulesTest {

  static final String BU = ".account";
  static final String OU = "org.example.hexagonal";
  static final String PACKAGE = OU + BU;

  static final String DOM = "." + BU + ".domain..";
  static final String APP = "." + BU + ".application..";
  static final String ADP = "." + BU + ".adapter..";
  static final String ADP_IN = "." + BU + ".adapter.in..";
  static final String ADP_OUT = "." + BU + ".adapter.out..";

  @Test
  void domainLayerDoesNotDependOnApplicationLayer() {
    noClasses().that()
      .resideInAPackage(DOM)
      .should()
      .dependOnClassesThat()
      .resideInAnyPackage(APP)
      .because("domain layer does not depend on application layer")
      .check(new ClassFileImporter()
        .importPackages(".."));
  }

  @Test
  void domainLayerDoesNotDependOnAdapterLayer() {
    noClasses().that()
      .resideInAPackage(DOM)
      .should()
      .dependOnClassesThat()
      .resideInAnyPackage(ADP)
      .because("domain layer does not depend on adapter layer")
      .check(new ClassFileImporter()
        .importPackages(".."));
  }

  @Test
  void applicationLayerDoesNotDependOnAdapterLayer() {
    noClasses().that()
      .resideInAPackage(APP)
      .should()
      .dependOnClassesThat()
      .resideInAnyPackage(ADP)
      .because("application layer does not depend on adapter layer")
      .check(new ClassFileImporter()
        .importPackages(".."));
  }

  @Test
  void adapterInLayerDoesNotDependOnAdapterOutLayer() {
    noClasses().that()
      .resideInAPackage(ADP_IN)
      .should()
      .dependOnClassesThat()
      .resideInAPackage(ADP_OUT)
      .because("adapter-in layer does not depend on adapter out layer")
      .check(new ClassFileImporter()
        .importPackages(".."));
  }

  @Test
  void adapterOutLayerDoesNotDependOnAdapterInLayer() {
    noClasses().that()
      .resideInAPackage(ADP_OUT)
      .should()
      .dependOnClassesThat()
      .resideInAPackage(ADP_IN)
      .because("adapter-out layer does not depend on adapter in layer")
      .check(new ClassFileImporter()
        .importPackages(".."));
  }


  // TODO: to be discussed! how about the common xml date adapters and the common avro mappers?
//  @Test
  void commonPackageDoesOnlyContainAbstractOrInterfaceClasses() {
    classes().that()
      .resideInAPackage("..common..")
      .should()
      .beInterfaces().orShould().haveModifier(JavaModifier.ABSTRACT)
      .because("common package does only contain abstract or interface classes")
      .check(new ClassFileImporter()
        .importPackages(".."));
  }

}
