package org.example.hexagonal;

import com.tngtech.archunit.core.importer.ClassFileImporter;

import org.example.hexagonal.archunit.HexagonalArchitecture;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;

class DependencyRuleTests {

	@Test
	void validateRegistrationContextArchitecture() {
		HexagonalArchitecture.boundedContext("org.example.hexagonal.account")

				.withDomainLayer("domain")

				.withAdaptersLayer("adapter")
				.incoming("in.web")
				.outgoing("out.persistence")
				.and()

				.withApplicationLayer("application")
				.services("service")
				.incomingPorts("port.in")
				.outgoingPorts("port.out")
				.and()

				.withConfiguration("configuration")
				.check(new ClassFileImporter()
						.importPackages("org.example.hexagonal.."));
	}

	@Test
	void testPackageDependencies() {
		noClasses().that()
				.resideInAPackage("org.example.hexagonal.domain..")
				.should()
				.dependOnClassesThat()
				.resideInAnyPackage("org.example.hexagonal.application..")
				.check(new ClassFileImporter()
						.importPackages("org.example.hexagonal.."));
	}

}
