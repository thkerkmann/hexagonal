package org.example.hexagonal.account.application.service;

import java.time.LocalDateTime;

import org.example.hexagonal.account.application.port.in.GetAccountBalanceQuery;
import org.example.hexagonal.account.application.port.out.LoadAccountPort;
import org.example.hexagonal.account.domain.Money;
import org.example.hexagonal.account.domain.Account.AccountId;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
// @Service
class GetAccountBalanceService implements GetAccountBalanceQuery {

	private final LoadAccountPort loadAccountPort;

	@Override
	public Money getAccountBalance(AccountId accountId) {
		return loadAccountPort.loadAccount(accountId, LocalDateTime.now())
				.calculateBalance();
	}
}
