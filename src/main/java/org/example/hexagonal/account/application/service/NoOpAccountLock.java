package org.example.hexagonal.account.application.service;

import org.example.hexagonal.account.application.port.out.AccountLock;
import org.example.hexagonal.account.domain.Account.AccountId;
import org.springframework.stereotype.Component;

@Component
class NoOpAccountLock implements AccountLock {

	@Override
	public void lockAccount(AccountId accountId) {
		// do nothing
	}

	@Override
	public void releaseAccount(AccountId accountId) {
		// do nothing
	}

}
