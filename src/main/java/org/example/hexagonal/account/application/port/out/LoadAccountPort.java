package org.example.hexagonal.account.application.port.out;

import java.time.LocalDateTime;

import org.example.hexagonal.account.domain.Account;
import org.example.hexagonal.account.domain.Account.AccountId;

public interface LoadAccountPort {

	Account loadAccount(AccountId accountId, LocalDateTime baselineDate);
}
