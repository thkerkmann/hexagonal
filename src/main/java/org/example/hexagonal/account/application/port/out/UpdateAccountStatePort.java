package org.example.hexagonal.account.application.port.out;

import org.example.hexagonal.account.domain.Account;

public interface UpdateAccountStatePort {

	void updateActivities(Account account);

}
