package org.example.hexagonal.account.application.port.out;

import org.example.hexagonal.account.domain.Account;

public interface AccountLock {

	void lockAccount(Account.AccountId accountId);

	void releaseAccount(Account.AccountId accountId);

}
