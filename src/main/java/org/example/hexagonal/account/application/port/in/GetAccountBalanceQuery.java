package org.example.hexagonal.account.application.port.in;

import org.example.hexagonal.account.domain.Money;
import org.example.hexagonal.account.domain.Account.AccountId;

public interface GetAccountBalanceQuery {

	Money getAccountBalance(AccountId accountId);

}
