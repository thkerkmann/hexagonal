FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /app
COPY /target/*.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]

